
package astrog1;

import astrog1.app.App;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maria
 */
public class AstroG1 {
    static final String PROJECT_NAME=AstroG1.class.getSimpleName();
    static App app=new App();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(PROJECT_NAME+" App start...");
        Thread th = new Thread(app);
        th.start();
        try {
            th.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(AstroG1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
