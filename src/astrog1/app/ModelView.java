
package astrog1.app;

/**
 *
 * @author maria
 */
public class ModelView {
    JFrameAstroG1 jfr;
    static public final int SLIDER_NUMBERS=6;
    //La prima linea è il numero centrale dell' alieno, 
    //la seconda la posizione del numero centrale dell' umano
    static final public int[][] SLIDER_COORD = {  {140, 224}, {330, 224}, {525, 224},
                                                    {140, 575}, {330, 575}, {525, 575}
    };
    static public final int DELTA_Y_LINEA_1=65;
    static public final int DELTA_Y_LINEA_2=80;
    static public int[] ndx=new int[SLIDER_NUMBERS];  //Numero di slider
    static final public String[] SLIDER_VALUES={"0","1","2","3","4","5","6","7","8","9"};
    
    public ModelView(JFrameAstroG1 jfr) {
        this.jfr = jfr;
    }
    
    public void init(){
        for (int i=0; i<SLIDER_NUMBERS ; i++)
            ndx[i] = 1;
    }
}
