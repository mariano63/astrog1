/*
 
 */
package astrog1.app;

/**
 *
 * @author maria
 */
public class App implements Runnable{
    JFrameAstroG1 frm;
    ModelView mv;
    
    
    @Override
    public void run() {
        frm=new JFrameAstroG1();
        mv=new ModelView(frm);
        mv.init();
        frm.setVisible(true);
    }

}
